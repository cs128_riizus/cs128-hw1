package main

import (
	"net/http"
	"github.com/gorilla/mux"
	"log"
)

func main() {

	router := mux.NewRouter()

	router.HandleFunc("/hello", GetHello).Methods("GET")
	router.HandleFunc("/check", GetCheck).Methods("GET")
	router.HandleFunc("/check", PostCheck).Methods("POST")
	router.HandleFunc("/check", PutCheck).Methods("PUT")

	log.Fatal(http.ListenAndServe(":8080", router))
}

func GetHello(w http.ResponseWriter, r *http.Request ){
	name := r.URL.Query().Get("name")
	if (name != ""){
		w.Write(([]byte)("Hello "+name+"!"))
	}else{
		w.Write(([]byte)("Hello user!"))
	}
}

func PostCheck(w http.ResponseWriter, r *http.Request ){
	w.WriteHeader(http.StatusOK)
	w.Write(([]byte)("This is a POST request"))
}

func GetCheck(w http.ResponseWriter, r *http.Request ){
	w.WriteHeader(http.StatusOK)
	w.Write(([]byte)("This is a GET request"))
}

func PutCheck(w http.ResponseWriter, r *http.Request ){
	w.WriteHeader(405)
}
