FROM golang:1.8-onbuild

RUN go get github.com/gorilla/mux

EXPOSE 8080
